package leixx.tmdb_browser;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.HashMap;

import leixx.tmdb_browser.fragments.MovieFragment;
import leixx.tmdb_browser.structures.TMDbAPIListQuerySpecification;

/**
 * Created by leixx on 10/26/17.
 */

/**
 * Adapter to create 4 main tabs (4 fragments) to be displayed in MainActivity.
 *
 */
public class TabPagerAdapter extends FragmentPagerAdapter {
    private String tabTitles[] = new String[] { "MOVIES", "TV SHOWS", "PEOPLE" };
    private final static int PAGE_COUNT = 3;
    private Context context;

    public TabPagerAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
//            case 0: {
//                return DiscoverFragment.newInstance("MainActivity");
//            }
            case 0:
//                Log.d("FRAGMENT", "fragment added");
                return MovieFragment.newInstance(
                    new TMDbAPIListQuerySpecification(context, "movie", "popular",
                            new HashMap<String,String>()), 1);
//            case 1: return new TVShowsFragment();
//            case 2: return PersonFragment.newInstance(3);
            default:
                return MovieFragment.newInstance(
                        new TMDbAPIListQuerySpecification(context, "movie", "popular",
                                new HashMap<String,String>()), 1);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate name based on item position
        return tabTitles[position];
    }



}
