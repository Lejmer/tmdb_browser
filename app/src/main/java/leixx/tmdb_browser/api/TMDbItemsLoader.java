package leixx.tmdb_browser.api;

import android.support.v7.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.net.URL;

import leixx.tmdb_browser.containers.TMDbItemRecyclerViewAdapter;
import leixx.tmdb_browser.structures.TMDbItemType;
import leixx.tmdb_browser.structures.TMDbAPIListQuerySpecification;

/**
 * Created by leixx on 10/28/17.
 */

/**
 * TODO is this method necessary? Won't it be better to merge its one method with ScrollListener?
 */
public class TMDbItemsLoader {
    private int page;
    int loadedItemsCount;
    private WeakReference<RecyclerView> rvRef;

    TMDbItemType type;

    public TMDbItemsLoader(int page, int loadedItemsCount, RecyclerView view, TMDbItemType type) {
        this.page = page;
        this.loadedItemsCount = loadedItemsCount;
        this.rvRef = new WeakReference<RecyclerView>(view);
        this.type = type;
        // construct the adapter's query URL from specification and pass it to loadTMDbItemsFromAPI
    }

    public void loadNewItems() throws NoAdapterSetException {
        TMDbItemRecyclerViewAdapter adapter = (TMDbItemRecyclerViewAdapter) rvRef.get().getAdapter();
        if (adapter == null)
            throw new NoAdapterSetException("Attempted to load new items without the adapter being set.");

        TMDbAPIListQuerySpecification specification = adapter.getSpecification();
        specification.setPage(String.valueOf(page));
        URL queryURL = specification.construct();
        new loadTMDbItemsFromAPI(this, rvRef).execute(queryURL);
    }

    public class NoAdapterSetException extends Exception {
        NoAdapterSetException(String message) {
            super(message);
        }
    }

    public TMDbItemType getType() {
        return type;
    }

}
