package leixx.tmdb_browser.containers;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import leixx.tmdb_browser.fragments.EndlessRecyclerViewScrollListener;
import leixx.tmdb_browser.api.TMDbItemsLoader;
import leixx.tmdb_browser.structures.TMDbItemType;

/**
 * Created by leixx on 10/27/17.
 */

/**
 * Listener that queries the API to loadNewItems another page of TMDbItems as user scrolls down
 */
public class TMDBItemScrollListener extends EndlessRecyclerViewScrollListener {
    /**
     * specify which layouts are acceptable for this RecycleView - add the constructors from parent
     */
    public TMDBItemScrollListener(LinearLayoutManager layoutManager) {
        super(layoutManager);
    }

    public TMDBItemScrollListener(GridLayoutManager layoutManager) {
        super(layoutManager);
    }

    /**
     * initialize the adapter with first batch of data before the user is able to scroll
     * and trigger onLoadMore by himself
     * @param view this view's adapter will be used
     */
    public void initialize(RecyclerView view) {
        this.currentPage = 1;
        this.previousLoadedItemCount = 0;
        onLoadMore(1,0,view);
    }

    @Override
    public void onLoadMore(int page, int loadedItemsCount, RecyclerView view) {
        try {
            new TMDbItemsLoader(page, loadedItemsCount, view, TMDbItemType.MOVIE)
                    .loadNewItems();
        } catch (TMDbItemsLoader.NoAdapterSetException e) {
            e.printStackTrace();
            // always set an adapter first before trying to load data!
        }
    }


}