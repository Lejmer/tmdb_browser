package leixx.tmdb_browser.containers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import leixx.tmdb_browser.R;
import leixx.tmdb_browser.fragments.MovieFragment;
import leixx.tmdb_browser.structures.TMDbAPIListQuerySpecification;
import leixx.tmdb_browser.structures.TMDbItem;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link TMDbItem} and makes a call to the
 * specified {@link MovieFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class TMDbItemRecyclerViewAdapter extends RecyclerView.Adapter<TMDbItemRecyclerViewAdapter.ViewHolder> {

    private final List<TMDbItem> TMDbItems;
    private final MovieFragment.OnListFragmentInteractionListener mListener;
    private TMDbAPIListQuerySpecification specification;


    public TMDbItemRecyclerViewAdapter(MovieFragment.OnListFragmentInteractionListener listener,
                                       TMDbAPIListQuerySpecification specification) {
        TMDbItems = new ArrayList<>();
        mListener = listener;
        this.specification = specification;
    }

    public void add(TMDbItem item) {
        TMDbItems.add(item);
    }

    public TMDbAPIListQuerySpecification getSpecification() {
        return this.specification;
    }

    public void setSpecification(TMDbAPIListQuerySpecification specification) {
        if (specification != this.specification) {
            this.specification = specification;
            resetContents();
        }
    }

    public void resetContents() {
        TMDbItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_movie, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.TMDbItem = TMDbItems.get(position);
        holder.mTitleView.setText(TMDbItems.get(position).title);
        holder.mDescriptionView.setText(TMDbItems.get(position).description);
        Picasso.with(specification.context)
                .load(TMDbItems.get(position).imgPath)
                .into(holder.mPosterView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.TMDbItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() { return TMDbItems.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleView;
        public final TextView mDescriptionView;
        public final ImageView mPosterView;

        public TMDbItem TMDbItem;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitleView = (TextView) view.findViewById(R.id.TMDBItem_title);
            mDescriptionView = (TextView) view.findViewById(R.id.TMDBItem_description);
            mPosterView = (ImageView) view.findViewById(R.id.TMDbItem_img);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitleView.getText() + "'";
        }
    }
}
