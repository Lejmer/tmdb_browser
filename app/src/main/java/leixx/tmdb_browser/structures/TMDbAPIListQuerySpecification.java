package leixx.tmdb_browser.structures;

/**
 * Created by leixx on 10/28/17.
 */

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import leixx.tmdb_browser.R;

/**
 * Structure to represent and construct GET requests for listings of objects from the API
 * Constructed queries have the following structure:
 * API_URL/{resource}/{action}?{parameter=value}&{parameter=value}&{API_KEY}
 *
 * Example constructed URI:
 * https://api.themoviedb.org/3/person/popular?&language=en-US&page=1&api_key=api_key
 *                               ^       ^          ^                    ^
 *                            resource action   parameters            API_KEY
 *
 * This structure does not control pagination - implement a {@link leixx.tmdb_browser.fragments.EndlessRecyclerViewScrollListener}
 * to handle pagination for you.
 * See https://developers.themoviedb.org/3 for full API specification
 */
public class TMDbAPIListQuerySpecification {
    public final String resource; // maybe not final? where could we implement resource switching?
    public String action;
    public final HashMap<String,String> parameters;
    public final Context context;


    public TMDbAPIListQuerySpecification(Context context, String resource, String action, HashMap<String, String> parameters) {
        this.context = context;
        this.resource = resource;
        this.action = action;
        this.parameters = parameters;
    }


    /**
     * Method useful for switching between sort criteria
     * @return action_id to use
     */
    public int getActionID() {
        switch(action) {
            case "popular":
                return 0;
            case "top_rated":
                return 1;
            case "now_playing": // Movies and TV Shows have different names for the same criterion
                return 2;
            case "on_the_air":
                return 2;
        }
        return -1;
    }

    /**
     * Method useful for switching between sort criteria
     * @param action_id to use
     */
    public void setAction(int action_id) {
        switch(action_id) {
            case 0:
                action = "popular";
                break;
            case 1:
                action = "top_rated";
                break;
            case 2:  // Movies and TV Shows have different names for the same criterion
                if (resource.equals("movie"))
                    action = "now_playing";
                else if (resource.equals("tv"))
                    action = "on_the_air";
                break;
        }
    }


    public void setPage(String page_no) {
        parameters.put("page", page_no);
    }

    /**
     * Constructs a URL ready to be used for querying the API.
     * Automatically adds API_KEY to the query
     * Does not control pagination - implement a {@link leixx.tmdb_browser.fragments.EndlessRecyclerViewScrollListener}
     * to handle pagination for you.
     *
     * @return a full URL constructed from object's attributes ready to be passed to {@link leixx.tmdb_browser.fragments.loadTMDbItemsFromAPI}
     *         or null if URL was malformed.
     */
    public URL construct()  {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(context.getString(R.string.API_AUTHORITY))
                .appendPath(context.getString(R.string.API_VERSION))
                .appendPath(resource)
                .appendPath(action)
                .appendQueryParameter("api_key", context.getString(R.string.API_KEY));

        for(Map.Entry<String, String> entry : parameters.entrySet())
            builder.appendQueryParameter(entry.getKey(), entry.getValue());

        try {
            return new URL(builder.build().toString());
        } catch (MalformedURLException e) {
            Log.e("URL build", "Failed to construct URL from query specification");
        }
        return null;
    }
}
