package leixx.tmdb_browser.structures;

/**
 * Created by leixx on 10/28/17.
 */

import android.net.Uri;

/**
 * An object displayed in MainActivity used to represent displayed Movies or TV Shows.
 * For displaying people use TMDbPerson.
 */
public class TMDbItem {
    public final int id;
    public final String title;
    public final String description;
    public final String starring;
    public final String genres;
    public final Uri imgPath;
    public final int year;
    public final double rating;


    public TMDbItem (int id, String title, String description, String starring,
                     String genres, Uri imgPath, int year, double rating){
        this.id = id;
        this.title = title;
        this.description = description;
        this.starring = starring;
        this.genres = genres;
        this.imgPath = imgPath;
        this.year = year;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return title;
    }
}
