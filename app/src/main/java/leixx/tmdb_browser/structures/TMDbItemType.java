package leixx.tmdb_browser.structures;

/**
 * Created by leixx on 10/29/17.
 */

public enum TMDbItemType {
    MOVIE,
    TV,
    PERSON
}
