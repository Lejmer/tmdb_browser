package leixx.tmdb_browser.structures;

/**
 * Created by leixx on 10/28/17.
 */

/**
 * An object displayed in MainActivity used to represent displayed People.
 */
public class TMDbPerson {
    public final String name;
    public final String featured_in;


    public TMDbPerson(String name, String featured_in){
        this.name = name;
        this.featured_in = featured_in;
    }

    @Override
    public String toString() {
        return featured_in;
    }
}
